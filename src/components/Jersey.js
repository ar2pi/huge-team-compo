import React, { Component } from 'react';
import PropTypes from 'prop-types';
import imgJerseyEmpty from '../assets/img/empty.png';
import imgJerseyFull from '../assets/img/full.png';

export const abbreviate = (names) => {
    const namesArr = names.trim().replace(/\s+/, ' ').split(' ').reverse();
    for (let i = 1; i < namesArr.length; i++) {
        namesArr[i] = namesArr[i].charAt(0) + '.';
    }
    return namesArr.reverse().join(' ');
};

export const getPosition = (row) => {
    switch (row) {
        case 0:
            return 'Archero';
        case 1:
            return 'Defensa';
        case 2:
            return 'Medio';
        case 3:
            return 'Delantero';
        default:
            return;
    }
};

class Jersey extends Component {
    static propTypes = {
        player: PropTypes.any,
        row: PropTypes.number.isRequired,
        column: PropTypes.number.isRequired,
        handlePlayerClick: PropTypes.func.isRequired
    }

    renderJersey = (player) => {
        if (player === null) {
            return <img alt="jersey empty" src={imgJerseyEmpty} className="jersey" />;
        } else {
            return <img alt="jersey full" src={imgJerseyFull} className="jersey" />;
        }
    };

    render() {
        return (
            <div
                className="jersey-wrapper"
                onClick={() => {
                    this.props.handlePlayerClick({
                        player: this.props.player,
                        row: this.props.row,
                        column: this.props.column
                    });
                }}
            >
                {this.renderJersey(this.props.player)}
                <small className="jersey-label">
                    {this.props.player !== null
                        ? abbreviate(this.props.player)
                        : getPosition(this.props.row)}
                </small>
            </div>
        );
    }
}

export default Jersey;
