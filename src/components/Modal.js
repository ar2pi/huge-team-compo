import React from 'react';

const Modal = (props) => (
    <div className="modal">
        {props.title && <h3 className="modal__title">{props.title}</h3>}
        <div className="modal__content">{props.children}</div>
    </div>
);

export default Modal;
