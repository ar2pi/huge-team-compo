import React from 'react';
import { mount } from 'enzyme';
import clone from 'lodash/clone';
import Field, { setPlayerRows, updatePlayerRow } from '../../components/Field';
import formations from '../fixtures/formations.json';
import players from '../fixtures/players.json';

const m442 = [[null], [null, null, null, null], [null, null, null, null], [null, null]];
const m433 = [[null], [null, null, null, null], [null, null, null], [null, null, null]];
const m334 = [[null], [null, null, null], [null, null, null], [null, null, null, null]];
const m4231 = [[null], [null, null, null, null], [null, null], [null, null, null], [null]];

test('should set player rows correctly for a given formation', () => {
    expect(setPlayerRows('4-4-2')).toEqual(m442);
    expect(setPlayerRows('4-3-3')).toEqual(m433);
    expect(setPlayerRows('3-3-4')).toEqual(m334);
    expect(setPlayerRows('4-2-3-1')).toEqual(m4231); // Giroud ftw!!
});

test('should update player position correctly in a given row', () => {
    let _m4231 = clone(m4231);
    updatePlayerRow({ player: 'O. Giroud', row: 4, column: 0 }, _m4231);
    expect(_m4231[4][0]).toBe('O. Giroud');
    updatePlayerRow({ player: 'H. Lloris', row: 0, column: 0 }, _m4231);
    expect(_m4231[0][0]).toBe('H. Lloris');
    updatePlayerRow({ player: 'K. Mbappé', row: 3, column: 0 }, _m4231);
    updatePlayerRow({ player: 'K. Mbappé', row: 3, column: 2 }, _m4231);
    expect(_m4231[3][2]).toBe('K. Mbappé');
    expect(_m4231).toEqual([
        ['H. Lloris'],
        [null, null, null, null],
        [null, null],
        [null, null, 'K. Mbappé'],
        ['O. Giroud']
    ]);
});

test('should render Field correctly with given formation', () => {
    const wrapper = mount(
        <Field formations={formations} players={players} />
    );
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.state('activeFormation')).toBe(formations[0]);
    expect(wrapper.state('playerRows')).toEqual(setPlayerRows(formations[0]));
});
