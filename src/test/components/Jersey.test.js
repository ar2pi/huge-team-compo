import React from 'react';
import { shallow } from 'enzyme';
import Jersey, { abbreviate } from '../../components/Jersey';

test('should abbreviate Jersey names correctly', () => {
    expect(abbreviate('John Ronald Reuel Tolkien')).toBe('J. R. R. Tolkien');
    expect(abbreviate('Kylian Mbappé')).toBe('K. Mbappé');
    expect(abbreviate('K  Mbappé')).toBe('K. Mbappé');
    expect(abbreviate(' Mbappé ')).toBe('Mbappé');
});

test('should render Jersey with correct name abbreviation', () => {
    const wrapper = shallow(
        <Jersey player="James Rodriguez" row={2} column={0} handlePlayerClick={() => 0} />
    );
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.jersey-label').text()).toBe('J. Rodriguez');
});

test('should render nameless Jersey correctly', () => {
    const wrapper = shallow(
        <Jersey player={null} row={2} column={0} handlePlayerClick={() => 0} />
    );
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.jersey-label').text()).toBe('Medio');
});
