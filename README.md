# huge-team-compo

### Install instructions

1.  `git clone git@gitlab.com:decksterr/huge-team-compo.git && cd huge-team-compo/`
2.  `npm install`
3.  `npm start`
4.  Head over to your browser at http://localhost:3000
5.  And... voilà ;)

### Run tests

-   `npm run test`

some inspiration: https://www.lequipe.fr/special/compose/coupe-du-monde-2018-france-croatie/
